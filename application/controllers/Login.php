<?php 
 
class Login extends CI_Controller{
 
	function __construct(){
		parent::__construct();		
		$this->load->model('m_login');
 
	}
 
	function index(){
		$this->load->view('v_login');
	}
 
	function aksi_login(){
		$code = $this->input->post('code');
		$where = array(
			'code' => $code,
			);
			
		$cek = $this->m_login->cek_login("data",$where)->num_rows();
		if($cek > 0){
			$data_user = $this->m_login->get_user_by_code($code);

			$data_session = array(
				'nama_lengkap' => $data_user['nama_lengkap'],
				'jabatan' => $data_user['jabatan'],
				'status' => 'login'
			);
 
			$this->session->set_userdata($data_session);
 
			redirect(base_url("index.php/admin"));
 
		} else {
			echo "Code anda tidak terdaftar / salah silahkan coba lagi!";
		}
	}
 
	function logout(){
		$this->session->sess_destroy();
		redirect(base_url('index.php/login'));
	}
}