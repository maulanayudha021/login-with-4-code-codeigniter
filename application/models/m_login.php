<?php 

class M_login extends CI_Model{	
	function cek_login($table,$where){		
		return $this->db->get_where($table,$where);
    }
    
    function get_user_by_code($code)
    { 
        $query = $this->db->get_where('data', array('code' => $code));
        return $query->row_array();
    }
}